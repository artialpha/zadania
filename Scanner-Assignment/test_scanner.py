from unittest import TestCase
from scanner import Scanner

list_of_documents = [
    ("""eyr:2029 iyr:2013
hcl:#ceb3a1 byr:1939 ecl:blu
hgt:163cm
pid:660456119

hcl:#0f8b2e ecl:grn
byr:1975 iyr:2011
eyr:2028 cid:207 hgt:158cm
pid:755567813

byr:2002 pid:867936514 eyr:2021 iyr:2012
hcl:#18171d ecl:brn cid:293 hgt:177cm

hgt:193cm
iyr:2010 pid:214278931 ecl:grn byr:1953
eyr:2021
hcl:#733820

iyr:2010 eyr:2020 hcl:#866857
byr:1934 pid:022785900
hgt:161cm ecl:oth

hgt:166cm
hcl:#602927
cid:262 ecl:brn pid:393738288
eyr:2021 byr:1928 iyr:2010

ecl:grn hcl:#6b5442
cid:317 byr:2001
eyr:2023 iyr:2016 pid:407685013
hgt:177cm

hcl:#86127d ecl:grn pid:113577635 iyr:2018
hgt:180cm eyr:2022 cid:59 byr:1921

byr:1984 eyr:2023
iyr:2015 hgt:152cm cid:177 ecl:amb
hcl:#fffffd pid:379600323""",
        [
            'eyr:2029 iyr:2013 hcl:#ceb3a1 byr:1939 ecl:blu hgt:163cm pid:660456119',
            'hcl:#0f8b2e ecl:grn byr:1975 iyr:2011 eyr:2028 cid:207 hgt:158cm pid:755567813',
            'byr:2002 pid:867936514 eyr:2021 iyr:2012 hcl:#18171d ecl:brn cid:293 hgt:177cm',
            'hgt:193cm iyr:2010 pid:214278931 ecl:grn byr:1953 eyr:2021 hcl:#733820',
            'iyr:2010 eyr:2020 hcl:#866857 byr:1934 pid:022785900 hgt:161cm ecl:oth',
            'hgt:166cm hcl:#602927 cid:262 ecl:brn pid:393738288 eyr:2021 byr:1928 iyr:2010',
            'ecl:grn hcl:#6b5442 cid:317 byr:2001 eyr:2023 iyr:2016 pid:407685013 hgt:177cm',
            'hcl:#86127d ecl:grn pid:113577635 iyr:2018 hgt:180cm eyr:2022 cid:59 byr:1921',
            'byr:1984 eyr:2023 iyr:2015 hgt:152cm cid:177 ecl:amb hcl:#fffffd pid:379600323',
        ])


]

list_one_doc = [
    ('eyr:2029 iyr:2013 hcl:#ceb3a1 byr:1939 ecl:blu hgt:163cm pid:660456119',
     {
         'eyr': '2029',
         'iyr': '2013',
         'hcl': '#ceb3a1',
         'byr': '1939',
         'ecl': 'blu',
         'hgt': '163cm',
         'pid': '660456119',
     })
]


list_check_values_correct = [
    ({'byr': 'asd', 'iyr': 'asd', 'eyr': 'asd', 'hgt': 'asd', 'hcl': 'asd', 'ecl': 'asd', 'pid': 'asd', 'cid': 'asd'},
     True),
    ({'byr': 'asd', 'iyr': 'asd', 'eyr': 'asd', 'hgt': 'asd', 'hcl': 'asd', 'ecl': 'asd', 'pid': 'asd'}, True),
    ({'byr': 'asd', 'iyr': 'asd', 'eyr': 'asd', 'hgt': 'asd', 'hcl': 'asd', 'ecl': 'asd'}, False),
    ({'byr': 'asd', 'iyr': 'asd', 'eyr': 'asd', 'hgt': 'asd', 'hcl': 'asd'}, False),
    ({'byr': 'asd', 'iyr': 'asd', 'eyr': 'asd', 'hgt': 'asd'}, False),
    ({'byr': 'asd', 'iyr': 'asd', 'eyr': 'asd'}, False),
    ({'byr': 'asd', 'iyr': 'asd'}, False),
    ({'byr': 'asd'}, False),
]

list_check_doc_correct = [
    ("""ecl:gry pid:860033327 eyr:2020 hcl:#fffffd byr:1937 iyr:2017 cid:147 hgt:183cm""", True),
    ('iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884hcl:#cfa07d byr:1929', False),
    ("""hcl:#ae17e1 iyr:2013 eyr:2024 ecl:brn pid:760753108 byr:1931 hgt:179cm""", True),
    ('hcl:#cfa07d eyr:2025 pid:166559648iyr:2011 ecl:brn hgt:59in', False)
]

list_how_many_are_correct = [
    ("""ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
byr:1937 iyr:2017 cid:147 hgt:183cm

iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884hcl:#cfa07d byr:1929

hcl:#ae17e1 iyr:2013
eyr:2024
ecl:brn pid:760753108 byr:1931
hgt:179cm

hcl:#cfa07d eyr:2025 pid:166559648iyr:2011 ecl:brn hgt:59in""", 2)
]

list_test_txt = [
    ("test_scanner\\test1.txt", 7),
    ("test_scanner\\test2.txt", 5),
    ("test_scanner\\test3.txt", 7),
    ("test_scanner\\test4.txt", 11),
    ("test_scanner\\test5.txt", 8),
    ("test_scanner\\test6.txt", 8),
]


class TestScanner(TestCase):
    def test_get_documents(self):
        documents = None
        scan = Scanner()
        for x in list_of_documents:
            print(x[0], '\n ^^ that\'s the string')
            lines = x[0].splitlines(True)
            print(lines, 'lines')
            documents = scan.get_documents(lines)
            for y in range(len(documents)):
                self.assertEqual(documents[y], x[1][y])

        print(documents)

    def test_get_value_from_doc(self):
        scan = Scanner()
        for x in list_one_doc:
            values = scan.get_value_from_doc(x[0])
            print(values)

            if 'eyr' in values:
                self.assertEqual(values['eyr'], x[1]['eyr'])

            if 'iyr' in values:
                self.assertEqual(values['iyr'], x[1]['iyr'])

            if 'hcl' in values:
                self.assertEqual(values['hcl'], x[1]['hcl'])

            if 'byr' in values:
                self.assertEqual(values['byr'], x[1]['byr'])

            if 'ecl' in values:
                self.assertEqual(values['ecl'], x[1]['ecl'])

            if 'hgt' in values:
                self.assertEqual(values['hgt'], x[1]['hgt'])

            if 'pid' in values:
                self.assertEqual(values['pid'], x[1]['pid'])

            if 'cid' in values:
                self.assertEqual(values['cid'], x[1]['cid'])

    def test_check_if_values_are_correct(self):
        scan = Scanner()
        for x in list_check_values_correct:
            self.assertEqual(scan.check_if_values_are_correct(x[0]), x[1])

    def test_check_if_doc_is_correct(self):
        scan = Scanner()
        for x in list_check_doc_correct:
            self.assertEqual(scan.check_if_doc_is_correct(x[0]), x[1])

    def test_check_how_many_doc_are_correct(self):
        scan = Scanner()
        for x in list_how_many_are_correct:
            documents = scan.get_documents(x[0].splitlines(True))
            self.assertEqual(scan.check_how_many_doc_are_correct(documents), x[1])

    def test_read_document(self):
        scan = Scanner()
        for x in list_test_txt:
            documents = scan.read_document(x[0])
            print(documents, '\nafter read')
            number = scan.check_how_many_doc_are_correct(documents)
            print(number, 'number of correct')
            self.assertEqual(scan.check_how_many_doc_are_correct(documents), x[1])


"""    def test_read_document(self):
        scan = Scanner-Assignment()
        scan.read_document()"""
