class Scanner:

    def __init__(self):
        self.necessary_values = ['byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid']

    def read_document(self, file):
        documents = None
        with open(file) as f:
            lines = f.readlines()
            #print(lines)
            documents = self.get_documents(lines)
        #for x in documents:
        #    print(x, '--------------------')
        #print(documents)
        return documents

    @staticmethod
    def get_documents(lines):
        documents = []
        document = ''
        for line in lines:
            if line == '\n':
                documents.append(document.lstrip(' '))
                document = ''
            document += ' ' + line.strip('\n')
        documents.append(document.lstrip(' '))

        return documents

    @staticmethod
    def get_value_from_doc(doc):
        values = {}
        doc = doc.split(' ')
        for val in doc:
            val = val.split(":")
            values[val[0]] = val[1]
        return values

    def check_if_values_are_correct(self, doc):
        return True if set(self.necessary_values) <= set(doc.keys()) else False

    def check_if_doc_is_correct(self, doc):
        values = self.get_value_from_doc(doc)
        #print(values, 'values')
        return self.check_if_values_are_correct(values)

    def check_how_many_doc_are_correct(self, documents):
        print(documents, '\n documents in function how many are correct')
        count = 0
        for doc in documents:
            #print(doc, 'doc in check how many')
            if self.check_if_doc_is_correct(doc):
                count += 1
        return count






