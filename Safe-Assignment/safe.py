class Safe:

    def __init__(self, password, range_numbers):
        self.password = password
        self.range_numbers = range_numbers
        self.message = ''

    def check_password(self):
        if isinstance(self.password, int) and\
                isinstance(self.range_numbers[0], int) and\
                isinstance(self.range_numbers[1], int):

            if self.two_digits(self.password) and\
                    self.increasing_or_even(self.password) and\
                    self.is_in_range(self.password, self.range_numbers):
                return True
        return False

    @staticmethod
    def two_digits(password):
        pass_str = str(password)

        for x in range(0, len(pass_str)-1):
            if pass_str[x] == pass_str[x+1]:
                print(password, 'two digit ok')
                return True
        print(password, 'TWO DIGIT NOT OK')
        return False

    @staticmethod
    def increasing_or_even(password):
        pass_str = str(password)
        for x in range(0, len(str(pass_str))-1):
            '''
            checking condition whether the next digit is smaller than the previous one
            is the same as checking whether the next  is the same or bigger
            '''
            if pass_str[x+1] < pass_str[x]:
                print(password, 'NOT INCREASING OR NOT EVEN')
                return False
        print(password, 'increasing or even ok')
        return True

    @staticmethod
    def is_in_range(password, range_numbers):
        if range_numbers[0] <= password <= range_numbers[1]:
            print(password, 'is in range ok', range_numbers)
            return True
        else:
            print(password, 'NOT IN RANGE', range_numbers)
            return False

    def __str__(self):
        return "password: " + str(self.password) + " and range: " \
               + str(self.range_numbers[0]) + "-" + str(self.range_numbers[1])

