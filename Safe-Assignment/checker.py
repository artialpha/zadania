from safe import Safe


class Checker:

    def __init__(self, range_numbers):
        self.safes = []
        self.solutions = []
        for x in range(range_numbers[0], range_numbers[1]+1):
            self.safes.append(Safe(x, range_numbers))

    def check_safes(self):
        for safe in self.safes:
            if safe.check_password():
                self.solutions.append(safe)

    def number_of_solutions(self):
        return len(self.solutions)

    def __str__(self):
        message = ''
        for x in self.solutions:
            message += 'correct: ' + x.__str__() + '\n'
        return message
