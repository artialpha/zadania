from unittest import TestCase

from safe import Safe


class TestSafe(TestCase):
    list_of_passwords_true_two_digit = [
        11,
        111,
        112,
        122,

        123455,  # at the end
        112345,  # at the beginning
        123345,  # in the middle

        1111,
        2222,
        3333,
        4444,
        5555,
        6666,
        7777,
        8888,
        9999,

        1223445667889,
        11233455677899,
        11122333,
        1234567899,
        1123456789,
        112233445566778899,
        10023,
    ]

    list_of_passwords_false_two_digit = [
        123,
        234,
        232,

        1023456789,
        1010101,
        101010,
    ]

    list_of_passwords_true_increasing = [
        123456789,
        234,
        2234,
        22345,
        3356,
        34478
    ]

    list_of_passwords_false_increasing = [
        321,
        3221,
        332211,
        99213,
        8778
    ]

    list_of_passwords_true_range = [
        (5, (0, 10)),
        (0, (-1, 1)),
        (1, (-100, 100)),
        (150, (100, 1000)),
        (-150, (-1000, -100))
    ]

    list_of_passwords_false_range = [
        (11, (0, 10)),
        (2, (-1, 1)),
        (120, (-100, 100)),
        (1111, (100, 1000)),
        (-1111, (-1000, -100))
    ]

    list_of_passwords_true_check = [
        (111111, (110012, 365579)),
        (112334, (110012, 365579)),
        (223344, (110012, 365579)),
        (333333, (110012, 365579)),
        (156678, (110012, 365579)),
        (234455, (110012, 365579)),
    ]

    list_of_passwords_false_check = [
        (223450, (110012, 365579)),
        (123789, (110012, 365579)),
        (55667788, (110012, 365579)),
    ]

    def test_two_digits(self):
        for x in self.list_of_passwords_true_two_digit:
            self.assertTrue(Safe.two_digits(x), "correct")

        for x in self.list_of_passwords_false_two_digit:
            self.assertFalse(Safe.two_digits(x), 'it\'s not correct')

    def test_increasing_or_even(self):
        for x in self.list_of_passwords_true_increasing:
            self.assertTrue(Safe.increasing_or_even(x), "correct")

        for x in self.list_of_passwords_false_increasing:
            self.assertFalse(Safe.increasing_or_even(x), 'it\'s not correct')

    def test_is_in_range(self):
        for x in self.list_of_passwords_true_range:
            self.assertTrue(Safe.is_in_range(x[0], x[1]), "correct")

        for x in self.list_of_passwords_false_range:
            self.assertFalse(Safe.is_in_range(x[0], x[1]), 'it\'s not correct')

    def test_check_password(self):
        safes_c = []
        safes_ic = []

        for x in self.list_of_passwords_true_check:
            safes_c.append(Safe(x[0], x[1]))

        for x in self.list_of_passwords_false_check:
            safes_ic.append(Safe(x[0], x[1]))

        for safe in safes_c:
            self.assertTrue(safe.check_password(), "correct")

        for safe in safes_ic:
            self.assertFalse(safe.check_password(), "incorrect")