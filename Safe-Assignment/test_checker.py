from unittest import TestCase
from checker import Checker


class TestChecker(TestCase):

    list_true = [
        ((1, 15), 1),
        ((5, 25), 2),
        ((7, 109), 9),
        ((0, 99), 9),
        ((100, 999), 81),
        ((0, 999), 90),
        ((1100, 1200), 45),
        ((1200, 1300), 15),
        ((1300, 1400), 13),
        ((100, 1000), 81),
        ((100, 200), 17),
        ((200, 300), 15),
        ((300, 400), 13),
        ((400, 500), 11),
        ((900, 1000), 1),
    ]

    def test_number_of_solutions(self):

        for x in self.list_true:
            ch = Checker(x[0])
            ch.check_safes()
            print(ch)
            self.assertEqual(ch.number_of_solutions(), x[1])
